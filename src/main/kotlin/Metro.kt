import dataclases.api.tmb.transit.ApiTMBTransit
import dataclases.api.tmb.transit.Features
import funcions.*
import io.ktor.client.call.*
import io.ktor.client.request.*

suspend fun metroMenu() {
    val metro: ApiTMBTransit = ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/metro?app_id=$app_id&app_key=$app_key").body()
    val metroFiltered = metro.features.sortedBy { it.properties?.CODILINIA }
    lateinit var choosedMetroLine: String
    lineBreak()
    for (i in 0..metroFiltered.lastIndex){
        if (i == 0){
            print("( Líneas disponibles: ".typography(false, true))
        }
        print("${metroFiltered[i].properties?.NOMLINIA} ".typography(metroFiltered[i].properties?.COLORLINIA, false, true))
        if (i == metroFiltered.lastIndex){ print(")".typography(false, true) + "\n") }
    }

    choosedMetroLine = scannerPrint("Introduce línea de metro.")

    val metroLine = parseTMBLineCode("metro", choosedMetroLine)
    if (metroLine == null) { showAnErrorScreen("metro") }
    val metroRecorregut: ApiTMBTransit = ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/metro/$metroLine/estacions?app_id=$app_id&app_key=$app_key").body()
    val hexColor = metroRecorregut.features.first().properties?.COLORLINIA

    var metroRecorregutFiltrat: List<Features>?
    metroRecorregutFiltrat = when(scannerPrint("\n[0] A destino ${metroRecorregut.features[0].properties?.ORIGENSERVEI}\n" + "[1] A destino ${metroRecorregut.features[0].properties?.DESTISERVEI}")){
        "0" -> metroRecorregut.features.sortedByDescending { it.properties?.ORDREESTACIO }
        else -> metroRecorregut.features.sortedBy { it.properties?.ORDREESTACIO }
    }

    for (i in 0..metroRecorregutFiltrat.lastIndex){
        when (i) {
            0 -> {
                println("\n[${metroRecorregutFiltrat[i].properties?.NOMLINIA}] ".typography(hexColor, true, false) + "${metroRecorregutFiltrat[i].properties?.DESCSERVEI}".typography(hexColor, false, false))
                println("┬ ".typography(hexColor) + "${metroRecorregutFiltrat[i].properties?.NOMESTACIO}  ".typography(hexColor, true,false) + "[${metroRecorregutFiltrat[i].properties?.CODIESTACIO}]".typography(false, true))
            }

            metroRecorregutFiltrat.lastIndex -> {
                println("│ " .typography(hexColor))
                print("┴ ".typography(hexColor) + "${metroRecorregutFiltrat[i].properties?.NOMESTACIO}  ".typography(hexColor, true,false) + "[${metroRecorregutFiltrat[i].properties?.CODIESTACIO}]".typography(false, true))
            }

            else -> {
                println("│ " .typography(hexColor))
                println("├ ".typography(hexColor) + "${metroRecorregutFiltrat[i].properties?.NOMESTACIO}  ".typography(false,false) + "[${metroRecorregutFiltrat[i].properties?.CODIESTACIO}]".typography(false, true))
            }
        }
    }

    val option = scannerPrint("\n\nSub-menú:\n[0]: Consultar salidas de una estación\n[1]: Salir")

    if (option == "0") {
        metroStationMenu(metroLine)
    } else {
        lineBreak(2)
        main()
    }
}

suspend fun metroStationMenu(metroLine: Int?) {
    if (metroLine == null) { showAnErrorScreen("metro") }
    val codigoEstacion = scannerPrint("\nIntroduce código de estación:")
    val accesosMetro: ApiTMBTransit = ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/metro/$metroLine/estacions/$codigoEstacion/accessos?app_id=$app_id&app_key=$app_key").body()

    lineBreak()
    for (i in 0..accesosMetro.features.lastIndex){
        if (i == 0) {
            println("Estación " + accesosMetro.features[i].properties?.NOMESTACIO.toString().typography(true, false))
            println("Salidas disponibles: ${accesosMetro.features.count()}".typography(false, true))
        }

        print("  • ".typography(true, false) + "Salida " + accesosMetro.features[i].properties?.NOMACCES)

        if (checkAdaptedExit(accesosMetro.features[i].properties?.NOMTIPUSACCESSIBILITAT)){ print(": " + "♿".typography(true, false) + " / ") }
        else { print(": " + "♿❌".typography(true, false) + " / ") }

        if (checkElevators(accesosMetro.features[i].properties?.NUMASCENSORS)){ print("\uD83D\uDED7".typography(true, false) + "\n") }
        else { print("\uD83D\uDED7❌".typography(true, false) + "\n") }
    }

    lineBreak(2)
    main()
}
