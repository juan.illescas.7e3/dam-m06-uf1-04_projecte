package funcions

import kotlin.math.absoluteValue

/**
 * Prints a line break. That's it.
 */
fun lineBreak(){
    print("\n")
}

/**
 * Prints one (or more) line breaks.
 * @param numberOfLinesBreaks Number of line breaks to execute. (In case if the number is set to zero (0), it will execute only one line break)
 */
fun lineBreak(numberOfLinesBreaks: Int){
    if (numberOfLinesBreaks.absoluteValue < 1) {
        repeat(numberOfLinesBreaks.absoluteValue) {
            print("\n")
        }
    } else {
        print("\n")
    }
}