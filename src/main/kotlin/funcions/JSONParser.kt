package funcions

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

val ParseJSONFromHTTPClient = HttpClient(CIO) {
    engine { requestTimeout = 0 }

    install(ContentNegotiation) {
        json( Json{ ignoreUnknownKeys = true } )
    }
}

val ParseJSONFromFileOrValue = Json {
    ignoreUnknownKeys = true
}