package funcions

/**
 * Parse anything into an Int value.
 * @return Int value of the object or 0 in case of Exception (object can't be converted to Int).
 */
fun Any.parseInt(): Int{
    return try {
        this.toString().toInt()
    } catch (e: NumberFormatException){
        0
    }
}