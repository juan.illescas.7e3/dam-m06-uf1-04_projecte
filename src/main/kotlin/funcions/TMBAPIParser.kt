package funcions

import app_id
import app_key
import dataclases.api.tmb.transit.ApiTMBTransit
import io.ktor.client.call.*
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking


val dadesBus: ApiTMBTransit = runBlocking {
    ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/bus?app_id=$app_id&app_key=$app_key").body()
}

val dadesMetro: ApiTMBTransit = runBlocking {
    ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/metro?app_id=$app_id&app_key=$app_key").body()
}

fun parseTMBLineColor(line: Int): String? {
    val filteredList = dadesBus.features.filter { it.properties?.CODILINIA == line }
    return try {
        return if (filteredList[0].properties?.COLORLINIA == null) {
            null
        } else {
            filteredList[0].properties?.COLORLINIA!!
        }
    } catch (e: Exception){
        null
    }
}

fun parseTMBLineCode(type: String, line: String): Int? {
    val list = when (type.lowercase()){
        "metro" -> dadesMetro
        "bus" -> dadesBus
        else -> throw Exception("Valor \"type\" debe ser \"metro\" o \"bus\"")
    }

    val filteredList = list.features.filter { it.properties?.NOMLINIA == line.uppercase() }
    return try {
        return if (filteredList[0].properties?.CODILINIA == null) {
            null
        } else {
            filteredList[0].properties?.CODILINIA!!
        }
    } catch (e: Exception){
        null
    }
}

fun checkAdaptedExit(nomTipusAccessibilitat: String?): Boolean{
    return nomTipusAccessibilitat != "No accessible"
}

fun checkElevators(numAscensors: Int?): Boolean{
    return numAscensors != 0
}