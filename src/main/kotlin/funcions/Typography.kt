package funcions


/**
 * Applies a user-customized color or effect to text output from the console using ANSI escape codes.
 * (Support for ANSI escape codes is not guaranteed on some devices.)
 *
 * @param [bold] Apply the bold effect to [this].
 * @param [italic] Apply the italic effect to [this].
 * @return [this] with the effect applied.
 */
fun String.typography(bold: Boolean?, italic: Boolean?): String{
    var init = ""
    when{
        (bold == true) -> init += "\u001B[1m"
        (italic == true) -> init += "\u001B[3m"
    }

    val end = "\u001B[0m"

    return init + this + end
}

/**
 * Applies a user-customized color or effect to text output from the console using ANSI escape codes.
 * (Support for ANSI escape codes is not guaranteed on some devices.)
 *
 * @param [bold] Apply the bold effect to [this].
 * @param [italic] Apply the italic effect to [this].
 * @param [R] Red RGB Code
 * @param [G] Green RGB Code
 * @param [B] Blue RGB Code
 * @return [this] with the effect applied.
 * @exception NumberFormatException RGB values ([R] [G] [B]) must be between 0 to 255.
 */
fun String.typography(R: Int, G: Int, B: Int): String{
    checkRGBValue(R, G, B)
    val init = "\u001B[38;2;${R};${G};${B}m"
    val end = "\u001B[0m"

    return init + this + end
}

/**
 * Applies a user-customized color or effect to text output from the console using ANSI escape codes.
 * (Support for ANSI escape codes is not guaranteed on some devices.)
 *
 * @param [hex] Web color (hex color)\n
 * ([hex] format can be "#FFFFFF" or "FFFFFF")
 * @return [this] with the effect applied.
 * @exception Exception [hex] value can not be empty.
 */
fun String.typography(hex: String?): String{
    if (hex == null) throw Exception("Hex value can not be null")

    val hexCode = hex.filterNot { it == '#' }
    val r: Int = hexCode.substring(0, 2).toInt(16)
    val g: Int = hexCode.substring(2, 4).toInt(16)
    val b: Int = hexCode.substring(4, 6).toInt(16)

    val init = "\u001B[38;2;${r};${g};${b}m"
    val end = "\u001B[0m"

    return init + this + end
}

/**
 * Applies a user-customized color or effect to text output from the console using ANSI escape codes.
 * (Support for ANSI escape codes is not guaranteed on some devices.)
 *
 * @param [hex] Web color (hex color)\n
 * @param [bold] Apply the bold effect to [this].
 * @param [italic] Apply the italic effect to [this].
 * ([hex] format can be "#FFFFFF" or "FFFFFF")
 * @return [this] with the effect applied.
 * @exception Exception [hex] value can not be empty.
 */
fun String.typography(hex: String?, bold: Boolean?, italic: Boolean?): String{
    if (hex == null) throw Exception("Hex value can not be null")

    val hexCode = hex.filterNot { it == '#' }
    val r: Int = hexCode.substring(0, 2).toInt(16)
    val g: Int = hexCode.substring(2, 4).toInt(16)
    val b: Int = hexCode.substring(4, 6).toInt(16)

    val init = "\u001B[38;2;${r};${g};${b}m"

    var effect = ""
    when{
        (bold == true) -> effect += "\u001B[1m"
        (italic == true) -> effect += "\u001B[3m"
    }

    val end = "\u001B[0m"

    return init + effect + this + end
}

/**
 * Applies a user-customized color or effect to text output from the console using ANSI escape codes.
 * (Support for ANSI escape codes is not guaranteed on some devices.)
 *
 * @param [bold] Apply the bold effect to [this].
 * @param [italic] Apply the italic effect to [this].
 * @param [R] Red RGB Code
 * @param [G] Green RGB Code
 * @param [B] Blue RGB Code
 * @return [this] with the effect applied.
 * @exception NumberFormatException RGB values ([R] [G] [B]) must be between 0 to 255.
 */
fun String.typography(R: Int, G: Int, B: Int, bold: Boolean?, italic: Boolean?): String{
    checkRGBValue(R, G, B)
    val init = "\u001B[38;2;${R};${G};${B}m"

    var effect = ""
    when{
        (bold == true) -> effect += "\u001B[1m"
        (italic == true) -> effect += "\u001B[3m"
    }

    val end = "\u001B[0m"

    return init + effect + this + end
}

private fun checkRGBValue(R: Int, G: Int, B: Int){
    if (R < 0 || R > 255) { throw NumberFormatException("RGB values must be only between 0 or 255") }
    else if (G < 0 || G > 255) { throw NumberFormatException("RGB values must be only between 0 or 255") }
    else if (B < 0 || B > 255) { throw NumberFormatException("RGB values must be only between 0 or 255") }
}