package funcions

import java.util.Scanner

/**
 * Prints the given [message] and the line separator to the standard output
 * stream, then, shows the scanner.
 *
 * The scanner finds and returns the next complete token from this scanner.
 * A complete token is preceded and followed by input that matches
 * the delimiter pattern. This method may block while waiting for input
 * to scan, even if a previous invocation of {@link #hasNext} returned
 * {@code true}.
 *
 * @return the next token
 * @throws NoSuchElementException if no more tokens are available
 * @throws IllegalStateException if this scanner is closed
 * @see java.util.Iterator
 */
fun scannerPrint(message: String): String{
    println(message)
    val scan = Scanner(System.`in`)
    return scan.next()
}