package funcions

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import main

suspend fun showAnErrorScreen(type: String){
    when(type.lowercase()) {
        "ibus" -> println("\n¡Error en la búsqueda! ".typography("FF0000", true, false) + "Asegúrese de que ha introducido bien el código de parada o que exista.".typography(false, true))
        "bus" -> println("\n¡Error en la búsqueda! ".typography("FF0000", true, false) + "Asegúrese de que la ruta de bus exista.".typography(false, true))
        "metro" -> println("\n¡Error en la búsqueda! ".typography("FF0000", true, false) + "Asegúrese de que la línea de metro exista.".typography(false, true))
        else -> println("\n¡Se ha producido un error desconocido!".typography("FF0000", true, false))
    }

    println("En unos instantes, volverá al menú principal...\n".typography(false, true))
    withContext(Dispatchers.IO) {
        Thread.sleep(3000)
    }

    main()
}