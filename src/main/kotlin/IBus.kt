import dataclases.api.tmb.ibus.APITMBiBus
import dataclases.api.tmb.transit.ApiTMBTransit
import funcions.*
import io.ktor.client.call.*
import io.ktor.client.request.*

suspend fun iBusMenu() {
    val codiParada = scannerPrint("\nIntroduzca código de parada: ")
    val iBusData: APITMBiBus = ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/ibus/stops/$codiParada?app_id=$app_id&app_key=$app_key").body()
    val iBusResults = iBusData.data?.iBus

    if (iBusResults?.isEmpty() == true) { showAnErrorScreen("ibus") }
    if (iBusResults != null) {
        for (i in 0..iBusResults.lastIndex){
            if (i == 0) { println("\n—— \uD83D\uDE8F\uD83D\uDE8D " + "Próximos autobuses".typography(true, false) + " ——") }
            println("Bus " + "${iBusResults[i].busLine}".typography(true, false) + " con destino " + "${iBusResults[i].destination}".typography(true, false) + ": Próximo autobús en " + "${iBusResults[i].incomingTime}".typography(true, false))
        }
    } else {
        showAnErrorScreen("ibus")
    }

    lineBreak(2)
    main()
}