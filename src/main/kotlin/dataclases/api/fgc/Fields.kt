package dataclases.api.fgc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Fields(
    @SerialName("stop_lon") var stopLon: Double? = null,
    @SerialName("route_url") var routeUrl: String? = null,
    @SerialName("route_text_color") var routeTextColor: String? = null,
    @SerialName("exception_type") var exceptionType: Int? = null,
    @SerialName("route_long_name") var routeLongName: String? = null,
    @SerialName("stop_name") var stopName: String? = null,
    @SerialName("stop_id") var stopId: String? = null,
    @SerialName("route_color") var routeColor: String? = null,
    @SerialName("route_type") var routeType: Int? = null,
    @SerialName("trip_headsign") var tripHeadsign: String? = null,
    @SerialName("arrival_time") var arrivalTime: String? = null,
    @SerialName("stop_sequence") var stopSequence: Int? = null,
    @SerialName("route_short_name") var routeShortName: String? = null,
    @SerialName("shape_id") var shapeId: Int? = null,
    @SerialName("wheelchair_boarding") var wheelchairBoarding: Int? = null,
    @SerialName("departure_time") var departureTime: String? = null,
    @SerialName("date") var date: String? = null,
    @SerialName("stop_lat") var stopLat: Double? = null,
    @SerialName("nom_linia") var nomLinia: String? = null,
    @SerialName("inicials") var inicials: String? = null,
    @SerialName("linia") var linia: Double? = null,
    @SerialName("estacio") var estacio: Double? = null,
    @SerialName("nom_estacio") var nomEstacio: String? = null,
    @SerialName("ordre") var ordre: Double? = null
)
