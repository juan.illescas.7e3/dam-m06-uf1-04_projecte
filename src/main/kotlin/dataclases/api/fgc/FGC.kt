package dataclases.api.fgc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class FGC(
    @SerialName("records") var records: ArrayList<Records>? = arrayListOf()
)
