package dataclases.api.fgc

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Records(
    @SerialName("datasetid") var datasetid: String? = null,
    @SerialName("recordid") var recordid: String? = null,
    @SerialName("fields") var fields: Fields? = null,
    @SerialName("record_timestamp") var recordTimestamp: String? = null
)
