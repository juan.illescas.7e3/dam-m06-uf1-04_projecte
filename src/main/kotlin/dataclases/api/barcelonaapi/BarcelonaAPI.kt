package dataclases.api.barcelonaapi

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BarcelonaAPI (
    @SerialName("data") var data: Data? = Data()
)