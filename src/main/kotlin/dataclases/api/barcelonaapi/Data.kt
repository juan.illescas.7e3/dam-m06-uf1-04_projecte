package dataclases.api.barcelonaapi

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Data (
    @SerialName("renfe") var renfe: ArrayList<Renfe> = arrayListOf(),
    @SerialName("bici") var bicing: ArrayList<Bicing> = arrayListOf()
)