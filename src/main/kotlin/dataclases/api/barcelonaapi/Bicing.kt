package dataclases.api.barcelonaapi

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Bicing(
    @SerialName("id") var id: String? = null,
    @SerialName("name") var name: String? = null,
    @SerialName("lat") var lat: String? = null,
    @SerialName("lon") var lon: String? = null,
    @SerialName("nearby_stations") var nearbyStations: String? = null
)
