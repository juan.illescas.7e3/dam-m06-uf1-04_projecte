package dataclases.api.barcelonaapi

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Renfe (
    @SerialName("id") var id: String? = null,
    @SerialName("operator") var operator: String? = null,
    @SerialName("type") var type: String? = null,
    @SerialName("line") var line: String? = null,
    @SerialName("name") var name: String? = null,
    @SerialName("zone") var zone: String? = null,
    @SerialName("lat") var lat: String? = null,
    @SerialName("lon") var lon: String? = null
)