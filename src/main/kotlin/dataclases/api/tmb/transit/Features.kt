package dataclases.api.tmb.transit

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Features (
    @SerialName("properties") var properties: Properties? = Properties()
)