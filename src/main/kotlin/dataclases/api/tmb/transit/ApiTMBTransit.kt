package dataclases.api.tmb.transit

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ApiTMBTransit (
    @SerialName("features") var features: ArrayList<Features> = arrayListOf()
)