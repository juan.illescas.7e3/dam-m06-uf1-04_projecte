package dataclases.api.tmb.ibus

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TMBiBus (
    @SerialName("routeId") var routeId: String? = null,
    @SerialName("line") var busLine: String? = null,
    @SerialName("text-ca") var incomingTime: String? = null,
    @SerialName("t-in-s") var timeInSeconds: Int? = null,
    @SerialName("destination") var destination: String? = null,
    @SerialName("t-in-min") var timeInMinutes: Int? = null
)