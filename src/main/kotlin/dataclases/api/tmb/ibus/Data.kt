package dataclases.api.tmb.ibus

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Data (
    @SerialName("ibus") var iBus: ArrayList<TMBiBus> = arrayListOf()
)