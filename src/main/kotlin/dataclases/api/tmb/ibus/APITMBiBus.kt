package dataclases.api.tmb.ibus

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class APITMBiBus (
    @SerialName("data") var data: Data? = Data()
)