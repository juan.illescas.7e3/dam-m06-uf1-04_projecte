package dataclases.gtfs

import kotlinx.serialization.Serializable

@Serializable
data class Agency(
    val agency_id: String? = null,
    val agency_name: String? = null,
    val agency_url: String? = null,
    val agency_timezone: String? = null,
    val agency_lang: String? = null,
    val agency_phone: String? = null
)
