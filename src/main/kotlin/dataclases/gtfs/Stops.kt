package dataclases.gtfs

import kotlinx.serialization.Serializable

@Serializable
data class Stops(
    val stop_id: Int?,
    val stop_name: String?,
    val stop_lat: Double?,
    val stop_lon: Double?
)
