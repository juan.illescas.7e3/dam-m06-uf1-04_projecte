package dataclases.gtfs

import kotlinx.serialization.Serializable

@Serializable
data class StopTimes(
    val trip_id: String?,
    val arrival_time: String?,
    val departure_time: String?,
    val stop_id: Int?,
    val stop_sequence: String?
)

//cuidao alerta que este archivo es pesado de cojones y tal vez pete