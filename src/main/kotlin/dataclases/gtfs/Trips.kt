package dataclases.gtfs

import kotlinx.serialization.Serializable

@Serializable
data class Trips(
    val route_id: String?,
    val service_id: String?,
    val trip_id: String?,
    val trip_headsign: String?
)
