package dataclases.gtfs

import kotlinx.serialization.Serializable

@Serializable
data class Calendar(
    val service_id: String?,
    val monday: Int?,
    val tuesday: Int?,
    val wednesday: Int?,
    val thursday: Int?,
    val friday: Int?,
    val saturday: Int?,
    val sunday: Int?,
    val start_date: Int?,
    val end_date: Int?
)
