import dataclases.api.fgc.FGC
import funcions.ParseJSONFromHTTPClient
import io.ktor.client.call.*
import io.ktor.client.request.*
import kotlinx.coroutines.runBlocking
import java.awt.Image
import java.io.File
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.imageio.ImageIO
import javax.swing.ImageIcon

fun getFGCNextTrainData(stopID: String): Array<Array<String>> {
    val horaActual = Calendar.getInstance().get(Calendar.HOUR_OF_DAY).toString()
        .padStart(2, '0') + ":" + Calendar.getInstance().get(Calendar.MINUTE)
        .toString() + ":" + Calendar.getInstance().get(Calendar.SECOND).toString()
    val sdf = SimpleDateFormat("HH:mm:ss", Locale.ROOT)
    val fgc: FGC = runBlocking{ ParseJSONFromHTTPClient.get("https://dadesobertes.fgc.cat/api/records/1.0/search/?dataset=viajes-de-hoy&q=&rows=-1").body() }
    val listaFiltrada = fgc.records?.sortedBy { it.fields?.departureTime }?.filter { it.fields?.stopId == stopID.uppercase() }
    var showOnly = 5
    val destinations = Array<Array<String>>(5){ arrayOf("", "", "") }

    if (listaFiltrada != null) {
        for (i in 0..listaFiltrada.lastIndex) {
            if (sdf.parse(horaActual) <= sdf.parse(listaFiltrada[i].fields?.departureTime)) {
                when(showOnly) {
                    5 -> destinations[0] = arrayOf(
                        listaFiltrada[i].fields?.routeShortName.toString(),
                        listaFiltrada[i].fields?.tripHeadsign.toString(),
                        listaFiltrada[i].fields?.departureTime.toString()
                    )
                    4 -> destinations[1] = arrayOf(
                        listaFiltrada[i].fields?.routeShortName.toString(),
                        listaFiltrada[i].fields?.tripHeadsign.toString(),
                        listaFiltrada[i].fields?.departureTime.toString()
                    )
                    3 -> destinations[2] = arrayOf(
                        listaFiltrada[i].fields?.routeShortName.toString(),
                        listaFiltrada[i].fields?.tripHeadsign.toString(),
                        listaFiltrada[i].fields?.departureTime.toString()
                    )
                    2 -> destinations[3] = arrayOf(
                        listaFiltrada[i].fields?.routeShortName.toString(),
                        listaFiltrada[i].fields?.tripHeadsign.toString(),
                        listaFiltrada[i].fields?.departureTime.toString()
                    )
                    1 -> destinations[4] = arrayOf(
                        listaFiltrada[i].fields?.routeShortName.toString(),
                        listaFiltrada[i].fields?.tripHeadsign.toString(),
                        listaFiltrada[i].fields?.departureTime.toString()
                    )
                }
                showOnly--

                if (showOnly == 0) break
            }
        }
    }
    return destinations
}

fun getFGCStationCodes(): Map<String, String>{
    val data: FGC = runBlocking{ ParseJSONFromHTTPClient.get("https://dadesobertes.fgc.cat/api/records/1.0/search/?dataset=codigo-estaciones&q=&rows=-1").body() }
    val records = data.records
    val codisEstacio = mutableMapOf<String, String>()
    lateinit var inicialEstacio: String
    lateinit var nomEstacio: String

    if (records != null) {
        for (i in 0..records.lastIndex){
            if (records[i].fields?.inicials.toString() == "M4" || records[i].fields?.inicials.toString() == "GS" ||
                records[i].fields?.inicials.toString() == "JI" || records[i].fields?.inicials.toString() == "MP" ||
                records[i].fields?.inicials.toString() == "MT" || records[i].fields?.inicials.toString() == "JS" ||
                records[i].fields?.inicials.toString() == "CS" || records[i].fields?.inicials.toString() == "CI" ||
                records[i].fields?.inicials.toString() == "Y7" || records[i].fields?.inicials.toString() == "MM" ||
                records[i].fields?.inicials.toString() == "MM" || records[i].fields?.inicials.toString() == "Y8") {
                continue
            } else {
                inicialEstacio = records[i].fields?.inicials.toString()
                nomEstacio = records[i].fields?.nomEstacio.toString()
                codisEstacio[inicialEstacio] = nomEstacio
            }
        }
    }

    return codisEstacio
}

fun getFGCLineIcon(line: String): ImageIcon {
    val url: URL = when(line){
        "L6" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/L6_barcelona.svg/240px-L6_barcelona.svg.png")
        "L7" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/L7_barcelona.svg/240px-L7_barcelona.svg.png")
        "L8" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/L8_barcelona.svg/240px-L8_barcelona.svg.png")
        "L12" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/L12_barcelona.svg/240px-L12_barcelona.svg.png")
        "S1" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/S1_barcelona.svg/240px-S1_barcelona.svg.png")
        "S2" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/S2_barcelona.svg/240px-S2_barcelona.svg.png")
        "S3" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/S3_barcelona.svg/240px-S3_barcelona.svg.png")
        "S4" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/S4_barcelona.svg/240px-S4_barcelona.svg.png")
        "S5" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/S5_barcelona.svg/240px-S5_barcelona.svg.png")
        "S6" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/S6_barcelona.svg/240px-S6_barcelona.svg.png")
        "S7" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/7/7f/S7_barcelona.svg/240px-S7_barcelona.svg.png")
        "S8" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/S8_barcelona.svg/240px-S8_barcelona.svg.png")
        "S9" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/S9_barcelona.svg/240px-S9_barcelona.svg.png")
        "R5", "R53" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/R5_barcelona.svg/240px-R5_barcelona.svg.png")
        "R50" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/R50_barcelona.svg/240px-R50_barcelona.svg.png")
        "R6", "R63" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/R6_barcelona.svg/240px-R6_barcelona.svg.png")
        "R60" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/R60_barcelona.svg/240px-R60_barcelona.svg.png")
        "RL1" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/RL1_lleida.svg/240px-RL1_lleida.svg.png")
        "RL2" -> URL("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/RL2_lleida.svg/240px-RL2_lleida.svg.png")
        "SE" -> File("./src/main/resources/image/serveiEscolar.png").toURI().toURL()
        else -> File("./src/main/resources/image/FGC.png").toURI().toURL()

    }

    val image = ImageIO.read(url)
    return ImageIcon(ImageIcon(image).image.getScaledInstance(35, 35, Image.SCALE_DEFAULT))
}