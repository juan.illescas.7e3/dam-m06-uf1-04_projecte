import dataclases.api.tmb.transit.ApiTMBTransit
import funcions.*
import io.ktor.client.call.*
import io.ktor.client.request.*

suspend fun busMenu() {
    val linia = parseTMBLineCode("bus", scannerPrint("\nIntroduce ruta de bus: "))
    if (linia == null){ showAnErrorScreen("bus") }
    val recorregutBus: ApiTMBTransit = ParseJSONFromHTTPClient.get("https://api.tmb.cat/v1/transit/linies/bus/$linia/parades?app_id=$app_id&app_key=$app_key").body()
    val hexColor = linia?.let { parseTMBLineColor(it) }
    var filtrat = recorregutBus.features.sortedBy{ it.properties?.ORDRE }
    filtrat = if (scannerPrint(
            "\nSeleccione opción:" +
            "\n[I]: Ruta de ida " + "(hacia ${filtrat.filter { it.properties?.SENTIT == "A" }.last().properties?.NOMPARADA})".typography(false, true) +
            "\n[V]: Ruta de vuelta " + "(hacia ${filtrat.filter { it.properties?.SENTIT == "T" }.last().properties?.NOMPARADA})".typography(false, true)
        ).uppercase() == "I")
    {
        filtrat.filter { it.properties?.SENTIT == "A" }
    } else {
        filtrat.filter { it.properties?.SENTIT == "T" }
    }

    for (i in 0..filtrat.lastIndex){
        when (i) {
            0 -> {
                println("\n(${filtrat[i].properties?.NOMLINIA}) ".typography(hexColor, true, false) + "${filtrat[i].properties?.DESTISENTIT}".typography(true, false))
                print("┬ ".typography(hexColor))
            }

            filtrat.lastIndex -> {
                println("│ ".typography(hexColor) + "${filtrat[i].properties?.DISTANCIAPARANTERIOR}m" .typography(false, true))
                print("┴ ".typography(hexColor))
            }

            else -> {
                println("│ ".typography(hexColor) + "(${filtrat[i].properties?.DISTANCIAPARANTERIOR}m)" .typography(false, true))
                print("├ ".typography(hexColor))
            }
        }

        println("${filtrat[i].properties?.NOMPARADA} (${filtrat[i].properties?.CODIPARADA})".typography(true,false))
    }

    lineBreak(2)
    main()
}