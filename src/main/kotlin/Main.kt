import clases.KSwingFGCProgram
import funcions.*

suspend fun main() {
    val option = scannerPrint(
        "Selecciona opción:\n\n" +
        "[0] Consultar ruta de bus\n" +
        "[1] Servicio iBus\n" +
        "[2] Consultar línea de metro\n" +
        "[3] Consultar próximos trenes FGC (KSwing)\n" +
        //"[999] Test\n" +
        "[4] Salir"
    )

    when(option.parseInt()) {
        0 -> busMenu()
        1 -> iBusMenu()
        2 -> metroMenu()
        3 -> {
            lineBreak(2)
            KSwingFGCProgram("PC")
        }
        //999 -> test()
    }
}