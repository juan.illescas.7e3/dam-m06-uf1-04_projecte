package clases

import funcions.kSwingLogger
import funcions.lineBreak
import funcions.typography
import getFGCLineIcon
import getFGCNextTrainData
import getFGCStationCodes
import kotlinx.coroutines.runBlocking
import main
import java.awt.Dimension
import java.awt.Font
import java.awt.Image
import java.awt.Toolkit
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.util.*
import javax.swing.*
import javax.swing.table.DefaultTableCellRenderer
import javax.swing.table.DefaultTableModel

class KSwingFGCProgram(private val codiEstacio: String): JPanel() {
    //values
    //logos FGC
    private val fgcLogo = ImageIcon(
        ImageIcon(Objects.requireNonNull(javaClass.getResource("/image/FGC.png"))).image.getScaledInstance(
            128,
            128,
            Image.SCALE_DEFAULT
        )
    )
    private val fgc112 = ImageIcon(
        ImageIcon(Objects.requireNonNull(javaClass.getResource("/image/fgc112.png"))).image.getScaledInstance(
            773,
            113,
            Image.SCALE_DEFAULT
        )
    )

    private val nomsColumnesTaula = arrayOf("Linea:", "Destino:", "Salida:")
    private val contingutTaula = getFGCNextTrainData(codiEstacio)
    private val codisINomsDeEstacio = getFGCStationCodes() //Map<CodiEstació (String), NomEstació (String)>
    private val nomDeEstacio = arrayOfNulls<String>(codisINomsDeEstacio.size)
    private val nomsICodisDeEstacio = codisINomsDeEstacio.entries.associateBy({ it.value }) { it.key }


    //generate and construct components
    private var selectorEstacioFGC: JComboBox<String>
    private val picture = JLabel("")
    private val table = JTable(contingutTaula, nomsColumnesTaula)
    private var nextTrainTXT = JLabel("Próximas salidas (desde " + codisINomsDeEstacio[codiEstacio] + ")")
    private val finDeServicioTXT = JLabel("Fin del servicio")
    private val selectStationTXT = JLabel("Seleccione estación:")
    private val trenFGC = JLabel("")
    private val credits = JLabel("Programa creado por Joan Illescas")
    private val iconaLinia1 = JLabel("")
    private val iconaLinia2 = JLabel("")
    private val iconaLinia3 = JLabel("")
    private val iconaLinia4 = JLabel("")
    private val iconaLinia5 = JLabel("")

    init {
        kSwingLogger("Espere... Obteniendo datos de la API...")
        kSwingLogger("Esto puede tardar dependiendo de la velocidad de tu internet...".typography(false, true))

        for ((it, estacio) in codisINomsDeEstacio.values.withIndex()) {
            nomDeEstacio[it] = estacio
        }

        selectorEstacioFGC = JComboBox(nomDeEstacio)

        //adjust size and set layout
        preferredSize = Dimension(1020, 700)
        val columnModel = table.columnModel
        layout = null

        //add components
        add(picture)
        add(table)
        add(nextTrainTXT)
        add(selectorEstacioFGC)
        add(selectStationTXT)
        add(trenFGC)
        add(credits)
        add(iconaLinia1)
        add(iconaLinia2)
        add(iconaLinia3)
        add(iconaLinia4)
        add(iconaLinia5)

        //set component bounds (only needed by Absolute Positioning)
        //logo FGC
        picture.setBounds(20, 20, 128, 128)
        picture.icon = fgcLogo

        //Selector de estación
        selectorEstacioFGC.selectedItem = codisINomsDeEstacio[codiEstacio]
        selectorEstacioFGC.setBounds(180, 80, 830, 65)
        selectorEstacioFGC.font = Font("SansSerif", Font.BOLD, 24)

        //text "Próximos trenes (desde ...)"
        nextTrainTXT.setBounds(20, 180, 990, 90)
        nextTrainTXT.font = Font("SansSerif", Font.BOLD, 34)
        nextTrainTXT.verticalAlignment = SwingConstants.CENTER

        //taula de destinacions
        //TAULA
        table.setBounds(20, 270, 990, 255)
        table.rowHeight = 50
        table.font = Font("SansSerif", Font.PLAIN, 32)
        table.isEnabled = false //Taula només de lectura
        table.isOpaque = false //Taula sense fons (transparent)
        (table.getDefaultRenderer(Any::class.java) as DefaultTableCellRenderer).isOpaque = false //Taula sense fons (transparent)
        table.showVerticalLines = false //Taula sense linies verticals (transparent)

        //CEL·LES
        //mida de les columnes
        columnModel.getColumn(0).preferredWidth = 100
        columnModel.getColumn(1).preferredWidth = 655
        columnModel.getColumn(2).preferredWidth = 235

        //MODEL
        val model = DefaultTableModel(contingutTaula, nomsColumnesTaula)

        //DEBUG (Descomentar para debugar)
        //kSwingLogger("DEBUG: Tabla:\nNúmero de filas: ${table.rowCount}\nNúmero de columnas: ${table.columnCount}")

        //...................................

        //text "Seleccione estación:"
        selectStationTXT.setBounds(180, 25, 830, 55)
        selectStationTXT.font = Font("SansSerif", Font.ITALIC, 26)

        //dibuix del tren 112
        trenFGC.setBounds(260, 570, 773, 113)
        trenFGC.icon = fgc112

        //text crédits
        credits.setBounds(20, 570, 240, 115)

        //text "Fin del servicio"
        finDeServicioTXT.setBounds(400, 295, 300, 70)
        finDeServicioTXT.font = Font("SansSerif", Font.PLAIN, 34)

        //icones de linia
        //fila 1
        val liniaPos1 = table.getValueAt(0, 0).toString()
        if (liniaPos1.isEmpty()){
            kSwingLogger("AVISO: NO SE PUEDE CARGAR ICONO DE LÍNEA EN FILA 1".typography("#ffff00") + " | Motivo: No hay tren registrado")
            remove(iconaLinia1)
            model.rowCount = 0
            table.isVisible = false
            add(finDeServicioTXT)
        } else {
            table.setValueAt("", 0, 0)
            iconaLinia1.setBounds(30, 277, 35, 35)
            iconaLinia1.icon = getFGCLineIcon(liniaPos1)
        }

        //fila 2
        val liniaPos2 = table.getValueAt(1, 0).toString()
        if (liniaPos2.isEmpty()){
            kSwingLogger("AVISO: NO SE PUEDE CARGAR ICONO DE LÍNEA EN FILA 2".typography("#ffff00") + " | Motivo: No hay tren registrado")
            remove(iconaLinia2)
            model.rowCount = 1
        } else {
            table.setValueAt("", 1, 0)
            iconaLinia2.setBounds(30, 327, 35, 35)
            iconaLinia2.icon = getFGCLineIcon(liniaPos2)
        }

        //fila 3
        val liniaPos3 = table.getValueAt(2, 0).toString()
        if (liniaPos3.isEmpty()){
            kSwingLogger("AVISO: NO SE PUEDE CARGAR ICONO DE LÍNEA EN FILA 3".typography("#ffff00") + " | Motivo: No hay tren registrado")
            remove(iconaLinia3)
            model.rowCount = 2
        } else {
            table.setValueAt("", 2, 0)
            iconaLinia3.setBounds(30, 377, 35, 35)
            iconaLinia3.icon = getFGCLineIcon(liniaPos3)
        }

        //fila 4
        val liniaPos4 = table.getValueAt(3, 0).toString()
        if (liniaPos4.isEmpty()){
            kSwingLogger("AVISO: NO SE PUEDE CARGAR ICONO DE LÍNEA EN FILA 4".typography("#ffff00") + " | Motivo: No hay tren registrado")
            remove(iconaLinia4)
            model.rowCount = 3
        } else {
            table.setValueAt("", 3, 0)
            iconaLinia4.setBounds(30, 427, 35, 35)
            iconaLinia4.icon = getFGCLineIcon(liniaPos4)
        }

        //fila 5
        val liniaPos5 = table.getValueAt(4, 0).toString()
        if (liniaPos5.isEmpty()){
            kSwingLogger("AVISO: NO SE PUEDE CARGAR ICONO DE LÍNEA EN FILA 5".typography("#ffff00") + " | Motivo: No hay tren registrado")
            remove(iconaLinia5)
            model.rowCount = 4
        } else {
            table.setValueAt("", 4, 0)
            iconaLinia5.setBounds(30, 477, 35, 35)
            iconaLinia5.icon = getFGCLineIcon(liniaPos5)
        }


        val favicon = ImageIcon("./src/main/resources/image/FGC.png")
        if (!screenSizeChecker()) {
            runBlocking{ throwProgramError(ProgramException.SCREEN_RESOLUTION_TOO_SMALL) }
        } else {
            val frame = JFrame("Próximo tren FGC") //Título de la ventana
            frame.contentPane.add(this)
            frame.isVisible = true //Hacer visible el programa (una vez cargado)
            frame.isAlwaysOnTop = true  //Esto se activa y después se desactiva para...
            frame.isAlwaysOnTop = false //...mostrar el programa delante de la pantalla.
            frame.isResizable = false //Hacer que el programa no se pueda canviar de tamaño
            frame.iconImage = favicon.image //Poner icono al programa
            frame.pack()
            frame.defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE            //Esto lo que hace...
            frame.addWindowListener(object : WindowAdapter() {                  //...es que cuando...
                override fun windowClosing(e: WindowEvent) {                    //...el programa se...
                    kSwingLogger("Ventana cerrada. Volviendo al menú...\n\n")   //...cierre, no haga...
                    e.window.dispose()                                          //...acabar la aplicación...
                    runBlocking { main() }                                          //...si no que vuelva...
                }                                                               //...al menú de main para...
            })                                                                  //...seleccionar otra opción.

            selectorEstacioFGC.addActionListener {
                kSwingLogger("¡Ventana cerrada! En unos instantes se abrirá la nueva ventana.")
                frame.dispose()
                runBlocking { KSwingFGCProgram(nomsICodisDeEstacio[selectorEstacioFGC.selectedItem]!!) }
            }

            kSwingLogger("¡Ventana activada!") //Manda un mensaje a la consola diciendo que el programa ya cargó
        }
    }

    private fun screenSizeChecker(): Boolean { //No dejes cargar el programa si el
        val screenSize = Toolkit.getDefaultToolkit().screenSize
        val width = screenSize.getWidth()
        val height = screenSize.getHeight()
        return width > 1020 && height > 700
    }

    private suspend fun throwProgramError(type: ProgramException) {
        lateinit var ed: String
        when (type) {
            ProgramException.SCREEN_RESOLUTION_TOO_SMALL -> {
                ed = "Este programa no es compatible con su resolución de pantalla."
                kSwingLogger("ERROR DE PROGRAMA ($type): ".typography("#FF0000", true, false) + ed)
                kSwingLogger("Si no ve la ventana, use Alt+TAB (Command+TAB en MacOS) y busque el diálogo".typography(false, true))

                JOptionPane.showMessageDialog(
                    JFrame(),
                    ed,
                    "Error de programa ($type)",
                    JOptionPane.ERROR_MESSAGE
                )
            }

            ProgramException.UNKNOWN -> {
                ed = "Error desconocido."
                kSwingLogger("ERROR DE PROGRAMA ($type): ".typography("#FF0000", true, false) + ed)
                kSwingLogger("Si no ve la ventana, use Alt+TAB (Command+TAB en MacOS) y busque el diálogo".typography(false, true))

                JOptionPane.showMessageDialog(
                    JFrame(),
                    ed,
                    "Error de programa ($type)",
                    JOptionPane.ERROR_MESSAGE
                )
            }

            ProgramException.STATION_NOT_ALLOWED -> {
                ed = "Estación ($codiEstacio) no permitida."
                kSwingLogger("ERROR DE PROGRAMA ($type): ".typography("#FF0000", true, false) + ed)
                kSwingLogger("Si no ve la ventana, use Alt+TAB (Command+TAB en MacOS) y busque el diálogo".typography(false, true))

                JOptionPane.showMessageDialog(
                    JFrame(),
                    ed,
                    "Error de programa ($type)",
                    JOptionPane.ERROR_MESSAGE
                )
            }
        }

        kSwingLogger("Volviendo al inicio...".typography(false, true))
        lineBreak(2)
        main()
    }

    enum class ProgramException {
        SCREEN_RESOLUTION_TOO_SMALL, UNKNOWN, STATION_NOT_ALLOWED
    }
}