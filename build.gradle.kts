import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.10"
    kotlin("plugin.serialization") version "1.6.10"
    application
}

group = "org.jcibravo"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.junit.jupiter:junit-jupiter:5.9.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
    implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")
    implementation("com.google.code.gson:gson:2.9.0")
    implementation("org.json:json:20220924")
    implementation("io.ktor:ktor-client-content-negotiation:2.1.2")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.1.2")
    implementation("io.ktor:ktor-client-core:2.1.2")
    implementation("io.ktor:ktor-client-cio:2.1.2")
    implementation("io.ktor:ktor-client-serialization:2.1.2")
    implementation("io.ktor:ktor-client-content-negotiation:2.1.2")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.1.2")
    implementation("io.github.pdvrieze.xmlutil:core-jvm:0.84.3")
    implementation("io.github.pdvrieze.xmlutil:serialization-jvm:0.84.3")
    implementation("app.softwork:kotlinx-serialization-csv:0.0.6")
    implementation("app.softwork:kotlinx-serialization-flf:0.0.6")
    implementation("org.apache.commons:commons-csv:1.9.0")
    implementation("com.itextpdf:itextpdf:5.5.13.3")
    implementation("net.sourceforge.htmlunit:htmlunit:2.66.0")
    implementation("org.jsoup:jsoup:1.11.3")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = null
}

application {
    mainClass.set("MainKt")
}
